INSERT INTO `category` (label,description) VALUES ('Boeuf','Pour la viande de bœuf, nous sélectionnons toujours des animaux élevés en liberté sur des pâturages très verts réputés pour leur qualité. Nous privilégions les viandes françaises et les races Salers et Normande. La race Salers est issue du terroir du Cantal réputé pour sa haute qualité gustative. Elle produit une viande fine et persillée et également un lait de grande qualité. Nous n’utilisons que les muscles issus de l’aloyau. La race normande est également connue pour son lait, mais elle est reconnue des bouchers son engraissement favorable dans les prairies du Cotentin et du Calvados.');
INSERT INTO `category` (label,description) VALUES ('Veau','Notre veau est d’Origine Française car notre pays est réputé pour la qualité des races mises en élevage et pour le savoir-faire des éleveurs. ');
INSERT INTO `category` (label,description) VALUES ('Porc','Notre porc est d’Origine Française. Nous en proposons deux gammes, avec, en particulier, un Porc Label Rouge pour notre rayon de viande Haute Qualité. Un goût incomparable.');
INSERT INTO `category` (label,description) VALUES ('Volaille','Nous avons parcouru les Grandes Halles de la volaille pour sélectionner des produits de bonne qualité puis nous avons sélectionné des partenaires en France qui donnent à leurs volailles leurs lettres de noblesse.  Cette volaille si longtemps décriée est redevenue goûteuse, ferme, agréable à manger. Les professionnels ont compris qu’aujourd’hui un produit doit être goûteux équilibré pour satisfaire une clientèle devenue exigeante. Toute la découpe est réalisée dans nos ateliers, du magret canard au filet de dinde. Ce sont nos professionnels et uniquement eux qui soccupent de vous et de votre commande.');
INSERT INTO `category` (label,description) VALUES ('Charcuterie - Traiteur','Nos charcuteries, produits traiteurs et fromages proviennent toutes d’entreprises extérieures sélectionnées pour leur qualité et pour le respect des normes.');
INSERT INTO `category` (label,description) VALUES ('Épicerie fine','Une bonne viande révélera toutes ses saveurs avec un assaisonnement de qualité. Nous avons parcouru les épiceries du Marché de Rungis pour vous apporter le meilleur : des produits rares ou originaux, provenant de terroirs reconnus. Sels, poivres, champignons ou vinaigres... délicieusement indispensables !');

INSERT INTO `tva_rates` (taux) VALUES ('5');
INSERT INTO `tva_rates` (taux) VALUES ('20');

INSERT INTO `unit` (label) VALUES ('kg');
INSERT INTO `unit` (label) VALUES ('l');
INSERT INTO `unit` (label) VALUES ('g');
INSERT INTO `unit` (label) VALUES ('pièce');

INSERT INTO `statut` (label) VALUES ('en cours');
INSERT INTO `statut` (label) VALUES ('terminée');

INSERT INTO `user_type` (type) VALUES ('client');
INSERT INTO `user_type` (type) VALUES ('admin');

INSERT INTO `user` (name, surname, login, password, num_and_street_name,cp_and_city,phone,mail,registration_date,userType_id) VALUES ('Allio', 'Florian', 'fallio', '�`S8`�SP�<ƫ�����ۘ���O�4B��', '3 rue françois mansart', '78280 guyancourt','0633490552','florian.allio@gmail.com','2022-02-10',2);
INSERT INTO `user` (name, surname, login, password, num_and_street_name,cp_and_city,phone,mail,registration_date,userType_id) VALUES ('Nachar', 'Karen', 'knachar', '�z�����m��$�^c�:#�B�"�S�N� �', '140 rue du temple', '75004 paris','0607393914','karen.nachar@gmail.com','2022-02-10',1);
INSERT INTO `user` (name, surname, login, password, num_and_street_name,cp_and_city,phone,mail,registration_date,userType_id) VALUES ('Sarrola', 'Thibaut', 'tsarrola', 'x6�t!՞���X}��l�#/�;#��n��[', '153 rue de charenton', '75012 paris','0655730685','thibaut.sarrola@gmail.com','2022-02-10',1)

INSERT INTO `sub_category` (label,category_id) VALUES ('Bavette','1');
INSERT INTO `sub_category` (label,category_id) VALUES ('Cote de boeuf','1');
INSERT INTO `sub_category` (label,category_id) VALUES ('Entrecote','1');
INSERT INTO `sub_category` (label,category_id) VALUES ('Rôti','1');;

INSERT INTO `sub_category` (label,category_id) VALUES ('Carré de veau','2');
INSERT INTO `sub_category` (label,category_id) VALUES ('Cote de veau','2');
INSERT INTO `sub_category` (label,category_id) VALUES ('Escalope de veau','2');

INSERT INTO `sub_category` (label,category_id) VALUES ('Cote de porc','3');
INSERT INTO `sub_category` (label,category_id) VALUES ('Poitrine de porc','3');
INSERT INTO `sub_category` (label,category_id) VALUES ('Filet mignon de porc','3');

INSERT INTO `sub_category` (label,category_id) VALUES ('Cuisse de canard','4');
INSERT INTO `sub_category` (label,category_id) VALUES ('Filet de poulet','4');
INSERT INTO `sub_category` (label,category_id) VALUES ('Cuisse de pintade','4');

INSERT INTO `sub_category` (label,category_id) VALUES ('Charcuterie','5');
INSERT INTO `sub_category` (label,category_id) VALUES ('Traiteur','5');

INSERT INTO `sub_category` (label,category_id) VALUES ('Sirop','6');
INSERT INTO `sub_category` (label,category_id) VALUES ('Bocaux','6');
INSERT INTO `sub_category` (label,category_id) VALUES ('Thé et Café','6');


INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('300/320g','La bavette d’aloyau est située prés de la cuisse entre le faux filet et le rumsteck. C’est un quartier de lune que l’on tranche dans le sens des fibres. C’est une tranche un peu épaisse à grosses fibres. Elle est très goûteuse et saignante.','aloyau.jpg','bavette d’aloyau','5.9','4','1');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('300/320g','La renomemée de la race Angus est mondiale, et il n’est pas rare de trouver des pièces d’Angus à la carte des établissements gastronameiques les plus réputés. Une viande rare et persillée que nous vous proposons pour une durée limitée !','aloyau.jpg','bavette d’aloyau angus','10.8','4','1');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('450/500g','De la convivialité, rien que de la convivialité. L’excellence du choix de l’état d’engraissement nous offre un product tendre persillé et savoureux ; en effet, plus l’animal est âgé, plus sa viande est persillée.','cote_de_boeuf.jpg','cote de boeuf','13.95','4','2');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('900/1000g','Découvrez la saveur exceptionnelle d’une côte de boeuf de race Angus, l’une des races les plus réputées au monde.','cote_de_boeuf_angus.jpg','cote de boeuf angus','44.7','4','2');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('360/400g','Le tournedos est une tranche de boeuf assez épaisse entourée de barde. Il est principalement découpé dans les morceaux nobles : filet et rumsteck en particulier. Nous vous proposons ici un tournedos coupé dans le filet.','tournedos.jpg','Tournedos de filet de boeuf','22.25','4','2');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('460/500g','La reine des morceaux à griller. Cette tranche découpée dans le train de côtes doit être épaisse pour révéler toutes ses qualités. Comme la Côte de bœuf, elle provient du même morceau. Sa viande doit être grasse et persillée.','entrecote.jpg','entrecote','16.42','4','3');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('560/600g','La reine des morceaux à griller. Cette tranche découpée dans le train de côtes doit être épaisse pour révéler toutes ses qualités. Comme la Côte de bœuf, elle provient du même morceau. Sa viande doit être grasse et persillée.','entrecote.jpg','entrecote supérieur','24.35','4','3');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('900/1000g','Préparer un Rôti nécessite une connaissance parfaite de la tendreté d’un morceau. Il faut également de la dextérité pour ficeler ni trop ni pas assez cette belle pièce de viande. Le rôti, c’est un morceau épais taillé dans l’épaisseur d’un muscle tendre et entouré d’un morceau de lard. Il doit passer au four.','rosbif.jpg','Rosbif supérieur Charolais','26.91','4','4');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('560/600g','La reine des morceaux à griller. Cette tranche découpée dans le train de côtes doit être épaisse pour révéler toutes ses qualités. Comme la Côte de bœuf, elle provient du même morceau. Sa viande doit être grasse et persillée.','entrecote.jpg','entrecote supérieur','24.35','4','4');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('900/1000g','Ce rôti est confectionné à partir de carrés désossés. Remarquablement tendre et goûteux, c’est un product d’exception !','roti_longe.jpg','roti de longe de veau','34','4','5');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('2000/2200g','En boucherie, un carré est un morceau composé de plusieurs côtes.Contrairement aux rôtis, les os sont laissés sur le morceau, ce qui permet dedonner plus de goût à la viande lors de la cuisson.','carre_veau.jpg','carre de veau','55','4','5');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('420/440g','Cette côte est découpée  dans le carré de veau. Venant juste après le collier, une série de 13 cotes de veau dessine le dos de l’animal Nous avons sélectionné pour vous les 8 cotes premières ainsi que les cotes filets.','cote_veau.jpg','cote de veau','13.3','4','6');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('300/320g','Le t-bone est une découpe spécifique de la côte de veau, reconnaissable avec son os en forme de T. Particulièrement tendre et savoureux, ce morceau apprécié des connaisseurs pourra être poêlé et même cuit au barbecue','t_bone_veau.jpg','T bone de veau','8.46','4','6');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('380/420g','Cette escalope très fine est garnie d’une farce, roulée et ficelée pour devenir un petit rôti ou un melon aplati. Ces paupiettes sont aussi coquettes qu’appétissantes.','paupiette_veau.jpg','paupiette de veau','9.76','4','7');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('300/320g','C’est un tranche fine découpée uniquement dans la noix, la sous noix ou la noix pâtissière. C’est une viande tendre et très maigre. De cuisson rapide, elle se prête à bien des recettes.','escalope_veau.jpg','escalope de veau','11.46','4','7');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('380/400g','La côte échine et plus grasse et plus tendre que ses congénères de la longe. Mais ce qui fait son attrait, c’est ce goût si particulier du gras qui fond entre les noix de viande.','cote_porc_echine.jpg','Côte de porc échine','3.15','4','8');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('490/530g','Il ne faut jamais oublier qu’un porc label rouge c’est une autre viande. Son goût va vous surprendre ainsi que sa texture et sa couleur.','cote_premiere.jpg','Côte première','9.14','4','8');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('480/500g','Dans le porc, la poitrine est située le long du ventre de l’animal. De tissus élastiques et entourés de gras, c’est un plat d’hiver peu onéreux.','poitrine_demi_sel.jpg','Poitrine de porc demi-sel','5.14','4','9');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('480/500g','Dans le porc, la poitrine est située le long du ventre de l’animal. De tissus élastique et entourée de gras, c’est un plat peu onéreux.','poitrine_porc_tranchee.jpg','Poitrine de porc fraîche tranchée','6.92','4','9');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('390/410g','Le filet mignon se trouve sous le milieu et la pointe du filet. Détaché soigneusement il forme un petit muscle long aux fibres longues.','filet_mignon_porc.jpg','Filet mignon (Label rouge)','10.6','4','10'); 
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('480/500g','Il ne faut jamais oublier qu’un porc label rouge c’est une autre viande. Son goût va vous surprendre ainsi que sa texture et sa couleur. C’est un porc élevé en plein air, en ferme et en champs. Sa viande est donc plus moelleuse et plus gouteuse. Pris dans la palette, les cubes de porc sont maigre et sans tendons.','saute_porc.jpg','Sauté de porc (Label rouge)','4.86','4','10'); 

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('600/640g','C’est une cuisse de canard de 380 gr environ issu de canard maigre,  né , élevé et abattu en France, dont la carcasse est travaillée par nos soins. ','cuisse_canard.jpg','Cuisses de canard','10.10','4','11');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('200g','Cette cuisse charnue est issue de canards à foie gras élevés dans le Sud-Ouest. Déjà cuite, livrée sous-vide entourée de sa graisse, elle vous permettra de réaliser en un instant un plat succulent.','cuisse_canard_confite.jpg','Cuisse de canard confite (cuite)','7.13','4','11');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('700/800g','Le filet de canard est beaucoup moins gras que le magret de canard , car il ne provient pas d’un canard gavé. Une chair goûteuse et tendre, à découvrir.','filet_canard.jpg','Filet de canard','20.48','4','11');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('400/440g','C’est un blanc de poulet sans peau issu de notre découpe de poulet fermier label rouge des éleveurs de la champagne.','filet_poulet.jpg','Filet de poulet fermier Label Rouge','13.54','4','12');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('440/480g','Pour changer des cuisses de poulet, essayez les cuisses de pintade ! Elles sont savoureuses et s’accomodent de bien des manières','cuisse_pintade.jpg','cuisse de pintade','6.87','4','13');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('1200/1400g','Contrairement à ce que vous pouvez trouver en magasin, ce rôti de dinde et mis en filet chez nous, par nos soins.','roti_dinde.jpg','Rôti de dinde filet','24.49','4','13');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('240/260g','Les salaisons sèches, nous préférons les sélectionner en Espagne, là où le cochon est roi. En France, le chorizo est très mal préparé. Viande de porc et piment, une recette qui plaira à toute heure.','chorizo.jpg','Chorizo fer à cheval ','5.16','4','14');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('280/300g','Pur porc, pas trop sec, beaucoup de parfum avec un zeste de clou de girofle.','saussisson.jpg','Saucisson sec supérieur','6.8','4','14');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('600/700g','C’est simplement un jarret de porc désossé, cuit dans sa couenne dans un vrai bouillon aromatisé et formé dans un moule. Qui n’a jamais mangé sur le coin d’une table ce petit délice ?','jambonneau.jpg','Jambonneau cuit au bouillon','16.8','4','14');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('380/440g','Voilà la star dans votre assiette ! Cette andouillette est préparée suivant un rituel ancestral fait de techniques apprises et transmises de père en fils depuis plusieurs générations. Label exceptionnel de tradition, elle est uniquement à cuire au grill, bannissez tout autre mode de cuisson.','andouillette.jpg','Andouillette AAAAA','12.2','4','14');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('1360/1450g','On ne présente plus ce grand classique ! Pour des raisons de conservation des tomates, ce product ne peut être mis sous-vide.','tomate.jpg','Tomate farcie','21.14','4','15');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('3000/3400g','Nous avons séléctionné pour vous les meilleurs ingrédients pour une choucroute réussie pour 5 personnes ! Saucisse de Francfort, palette demi-sel, saucisse fumée, sans oublier la délicieuse choucroute IGP en provenance directe d’Alsace !','choucroute.jpg','Choucroute farcie d’Alsace','28.42','4','15');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('25cl','Ce sirop d’exception doit être conservé au frais après ouverture. Fabrication 100% artisanale en Vallée de Chevreuse – made in France. Sans arôme artificiel, ni arôme naturel. Sans colorant. Sans conservateur.','fenouil_citron_vert.jpg','Sirop artisanal Fenouil au Citron vert','9','4','16');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('25cl','Ce sirop d’exception doit être conservé au frais après ouverture. Fabrication 100% artisanale en Vallée de Chevreuse – made in France. Sans arôme artificiel, ni arôme naturel. Sans colorant. Sans conservateur.','basilic.jpg','Sirop artisanal Basilic','10','4','16');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('380g','Un grand classique de la cuisine française, viande de poule et riz de Camargue, une recette tendre et fondante.','blanquette.jpg','Blanquette de Volaille au Riz de Camargue','8','4','17');
INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('380g','La recette US originelle du chili con carne, revisitée par le Chef Michel Grobon, ancien Chef de Ronald Reagan à la Maison Blanche.','chili_con_carne.jpg','Chili con Carne','9','4','17');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('200g','Le café Bio Signature est parfaitement équilibré. Rond et aromatique il possède des notes chocolatées, végétales et épicées. C’est le café idéal pour découvrir Araku qui fonctionne avec tous les types de machine.','signature.jpg','Café signature','12','4','18');

INSERT INTO `product` (weight,description,image,name,ht_amount,unit_id,subcategory_id) VALUES ('100g','Ce thé vert, gourmand et fruité, évoque la datte verte, la fleur d’oranger, la rose et les fruits rouges','the.jpg','Thé du Hammam','9','4','18');

INSERT INTO `purshase` (total_amount_parsed,total_amount,purchase_date,statut_id,user_id) VALUES ('21.70','21.70','2021-08-18','1','1');
INSERT INTO `purshase` (total_amount_parsed,total_amount,purchase_date,statut_id,user_id) VALUES ('21.70','21.70','2014-11-02','2','2');

INSERT INTO `command_lign` (quantity,purshase_id,product_id) VALUES ('2','1','1');
INSERT INTO `command_lign` (quantity,purshase_id,product_id) VALUES ('1','1','22');
INSERT INTO `command_lign` (quantity,purshase_id,product_id) VALUES ('1','1','25');
INSERT INTO `command_lign` (quantity,purshase_id,product_id) VALUES ('2','2','1');
INSERT INTO `command_lign` (quantity,purshase_id,product_id) VALUES ('1','2','22');
INSERT INTO `command_lign` (quantity,purshase_id,product_id) VALUES ('1','2','25');

INSERT INTO `payment` (payment_date,method,amount,purshase_id) VALUES ('2022-02-10','CB','50','1');

INSERT INTO `invoice` (ht_amount,ttc_amount,tva_amount,num_and_street_name,purshase_id,tVARates_id,user_id) VALUES ('20','24','4','12 rue des allouettes','1','2','2');

