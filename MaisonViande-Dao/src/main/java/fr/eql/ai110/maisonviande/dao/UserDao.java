package fr.eql.ai110.maisonviande.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.User;
import fr.eql.ai110.maisonviande.idao.UserIDao;

@Remote(UserIDao.class)
@Stateless
public class UserDao extends GenericDao<User> implements UserIDao {

	@Override
	public User authenticate(String login, String password) {
		User user = null;
		List<User> users;
		Query query = em.createQuery("SELECT u FROM User u WHERE u.login = :loginParam "
				+ "AND u.password = :hashedPasswordParam");
		query.setParameter("loginParam", login);
		query.setParameter("hashedPasswordParam", password);
		users = query.getResultList();
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}

	@Override
	public String getSalt(String login) {
		Query query = em.createQuery("SELECT u.salt FROM User u WHERE u.login = :loginParam");
		query.setParameter("loginParam", login);
		String salt = (String)query.getSingleResult();
		System.out.println(salt);
		return salt;
	}

	@Override
	public List<Purshase> getPurshasesByUserId(User user) {
		String sqlQuery = "SELECT * FROM purshase p WHERE p.user_id = :userIdParam";
		
		Query query = em.createNativeQuery(sqlQuery, Purshase.class);
		query.setParameter("userIdParam", user.getId());
		List<Purshase> result = query.getResultList();

		return result;
	}


}
