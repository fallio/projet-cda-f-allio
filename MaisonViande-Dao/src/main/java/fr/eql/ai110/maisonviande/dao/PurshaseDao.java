package fr.eql.ai110.maisonviande.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.idao.PurshaseIDao;

@Remote(PurshaseIDao.class)
@Stateless
public class PurshaseDao extends GenericDao<Purshase> implements PurshaseIDao {

	
	@Override
	public Purshase add(Purshase t) {
		return super.add(t);
	}

	@Override
	public Purshase getLastEntry() {
		String sqlQuery = "SELECT * FROM purshase ORDER BY id DESC LIMIT 1";
		
		Query query = em.createNativeQuery(sqlQuery, Purshase.class);
		Purshase result = (Purshase) query.getSingleResult();
		return result;
	}

}
