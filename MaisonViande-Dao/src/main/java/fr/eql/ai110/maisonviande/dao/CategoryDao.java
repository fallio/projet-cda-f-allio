package fr.eql.ai110.maisonviande.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.idao.CategoryIDao;

@Remote(CategoryIDao.class)
@Stateless
public class CategoryDao extends GenericDao<Category> implements CategoryIDao {

	@Override
	public List<Category> getCategories() {
			Query query = em.createQuery("SELECT c FROM Category c");
			return (List<Category>) new ArrayList<Category>(query.getResultList());
	}

}
