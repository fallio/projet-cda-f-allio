package fr.eql.ai110.maisonviande.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.Unit;
import fr.eql.ai110.maisonviande.idao.UnitIDao;

@Remote(UnitIDao.class)
@Stateless
public class UnitDao extends GenericDao<Unit> implements UnitIDao{

	@Override
	public List<Unit> getAll() {
		return super.getAll();
	}
	
	

}
