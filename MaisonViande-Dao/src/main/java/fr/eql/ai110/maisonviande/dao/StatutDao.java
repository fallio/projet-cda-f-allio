package fr.eql.ai110.maisonviande.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.Statut;
import fr.eql.ai110.maisonviande.idao.StatutIDao;

@Remote(StatutIDao.class)
@Stateless
public class StatutDao extends GenericDao<Statut> implements StatutIDao {

	@Override
	public List<Statut> getAll() {
		return super.getAll();
	}

	
	
}
