package fr.eql.ai110.maisonviande.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.entity.Product;
import fr.eql.ai110.maisonviande.idao.ProductIDao;


@Remote(ProductIDao.class)
@Stateless
public class ProductDao extends GenericDao<Product> implements ProductIDao {

	@Override
	public List<Product> productsFromCategory(Category category) {
		List<Product> products;
		Query query = em.createQuery("SELECT p FROM Product p JOIN p.subCategory sc JOIN sc.category WHERE sc.category = :categoryParam");
		query.setParameter("categoryParam", category);
		products = query.getResultList();
		
		return products;
	}


}
