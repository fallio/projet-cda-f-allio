package fr.eql.ai110.maisonviande.dao;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.CommandLign;
import fr.eql.ai110.maisonviande.idao.CommandLignIDao;

@Remote(CommandLignIDao.class)
@Stateless
public class CommandLignDao extends GenericDao<CommandLign> implements CommandLignIDao {

	@Override
	public CommandLign add(CommandLign t) {
		return super.add(t);
	}

	

}
