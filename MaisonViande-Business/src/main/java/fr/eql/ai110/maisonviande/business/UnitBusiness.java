package fr.eql.ai110.maisonviande.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.CommandLign;
import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.Unit;
import fr.eql.ai110.maisonviande.ibusiness.PurshaseIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.StatutIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.UnitIBusiness;
import fr.eql.ai110.maisonviande.idao.CommandLignIDao;
import fr.eql.ai110.maisonviande.idao.PurshaseIDao;
import fr.eql.ai110.maisonviande.idao.UnitIDao;

@Remote(UnitIBusiness.class)
@Stateless
public class UnitBusiness implements UnitIBusiness {

	@EJB
	UnitIDao UnitDao;

	@Override
	public List<Unit> getUnits() {
		return UnitDao.getAll();
	}

	

	
	
	

}
