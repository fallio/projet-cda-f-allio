package fr.eql.ai110.maisonviande.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.entity.Product;
import fr.eql.ai110.maisonviande.ibusiness.ProductIBusiness;
import fr.eql.ai110.maisonviande.idao.ProductIDao;

@Remote(ProductIBusiness.class)
@Stateless
public class ProductBusiness implements ProductIBusiness{

	@EJB
	ProductIDao productDao;

	@Override
	public List<Product> getProductsFromCategory(Category category) {
		return productDao.productsFromCategory(category);
	}

	
	
}
