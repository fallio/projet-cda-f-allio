package fr.eql.ai110.maisonviande.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.User;
import fr.eql.ai110.maisonviande.ibusiness.UserIBusiness;
import fr.eql.ai110.maisonviande.idao.UserIDao;

@Remote(UserIBusiness.class)
@Stateless
public class UserBusiness implements UserIBusiness {

	@EJB
	UserIDao userDao;

	@Override
	public List<User> getAll() {
		return userDao.getAll();
	}

	@Override
	public void create(User user, String password) {
		

		// générer un hash à partir du password saisi
		String hashedPassword = generateHashedPassword(password);
		user.setPassword(hashedPassword);
		userDao.add(user);	//USES GENERIC METHOD
		System.out.println("Business create method with : " + user.getLogin());
	}

	private String generateHashedPassword(String password) {

		String hashedPassword = "";

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashedPassword;
	}

	@Override
	public User connect(String login, String password) {
		String hashedPassword = generateHashedPassword(password);	
		return userDao.authenticate(login, hashedPassword);
	}

	@Override
	public List<Purshase> getPurshasesByUser(User user) {
		return userDao.getPurshasesByUserId(user);
	}

	


}
