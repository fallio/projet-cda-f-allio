package fr.eql.ai110.maisonviande.business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.ibusiness.CategoryIBusiness;
import fr.eql.ai110.maisonviande.idao.CategoryIDao;

@Remote(CategoryIBusiness.class)
@Stateless
public class CategoryBusiness implements CategoryIBusiness {

	@EJB
	CategoryIDao categoryDao;
	
	@Override
	public List<Category> getCategories() {
		
		return categoryDao.getCategories();
	}

}
