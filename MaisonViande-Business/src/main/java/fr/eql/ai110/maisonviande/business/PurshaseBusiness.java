package fr.eql.ai110.maisonviande.business;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.maisonviande.entity.CommandLign;
import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.ibusiness.PurshaseIBusiness;
import fr.eql.ai110.maisonviande.idao.CommandLignIDao;
import fr.eql.ai110.maisonviande.idao.PurshaseIDao;

@Remote(PurshaseIBusiness.class)
@Stateless
public class PurshaseBusiness implements PurshaseIBusiness {

	@EJB
	PurshaseIDao purshaseDao;
	@EJB
	CommandLignIDao commandLignDao;

	@Override
	public void addPurshase(Purshase purshase) {
		purshaseDao.add(purshase);
		
	}

	@Override
	public void addCommandLign(CommandLign commandLign) {
		commandLignDao.add(commandLign);
	}

	@Override
	public Purshase getLastEntry() {
		return purshaseDao.getLastEntry();
	}

	@Override
	public List<Purshase> getAll() {
		return (List<Purshase>) purshaseDao.getAll();
	}

	@Override
	public String doubleTotalAmountParsed(Double totalAmount) {
	        String pattern = "0.00";
	        NumberFormat formatter =  new DecimalFormat(pattern);
	        String totalAmountParsed = formatter.format(totalAmount);
	        System.out.println("Formatted double d = "+totalAmountParsed);
		return totalAmountParsed;
	}

	
	
	
	

}
