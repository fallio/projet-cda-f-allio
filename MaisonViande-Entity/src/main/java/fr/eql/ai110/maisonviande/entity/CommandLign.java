package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "command_lign")
public class CommandLign implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="quantity")
	private Integer quantity;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Purshase purshase;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Product product;

	public CommandLign() {
		super();
	}

	public CommandLign(Integer id, Integer quantity, Purshase purshase, Product product) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.purshase = purshase;
		this.product = product;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Purshase getPurshase() {
		return purshase;
	}

	public void setPurshase(Purshase purshase) {
		this.purshase = purshase;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
}
