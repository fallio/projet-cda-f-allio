package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tva_rates")
public class TVARates implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="taux")
	private String taux;
	
	@OneToMany(mappedBy = "tVaRates", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Invoice> invoices;

	public TVARates() {
		super();
	}

	public TVARates(Integer id, String taux, List<Invoice> invoices) {
		super();
		this.id = id;
		this.taux = taux;
		this.invoices = invoices;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaux() {
		return taux;
	}

	public void setTaux(String taux) {
		this.taux = taux;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	
	
}
