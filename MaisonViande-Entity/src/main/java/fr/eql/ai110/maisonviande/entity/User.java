package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="surname")
	private String surname;
	@Column(name="login",unique = true)
	private String login;
	@Column(name="password")
	private String password;
	@Column(name="num_and_street_name")
	private String numAndStreetName;
	@Column(name="cp_and_city")
	private String cpAndCity;
	@Column(name="phone")
	private String phone;
	@Column(name="mail")
	private String mail;
	@Column(name="registration_date")
	private Date registrationDate;
	@Column(name = "salt")
	private String salt;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Invoice> invoices;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Purshase> purshases;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private UserType userType;

	public User() {
		super();
	}

	public User(Integer id, String name, String surname, String login, String password, String numAndStreetName,
			String cpAndCity, String phone, String mail, Date registrationDate, String salt, List<Invoice> invoices,
			List<Purshase> purshases, UserType userType) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.password = password;
		this.numAndStreetName = numAndStreetName;
		this.cpAndCity = cpAndCity;
		this.phone = phone;
		this.mail = mail;
		this.registrationDate = registrationDate;
		this.salt = salt;
		this.invoices = invoices;
		this.purshases = purshases;
		this.userType = userType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNumAndStreetName() {
		return numAndStreetName;
	}

	public void setNumAndStreetName(String numAndStreetName) {
		this.numAndStreetName = numAndStreetName;
	}

	public String getCpAndCity() {
		return cpAndCity;
	}

	public void setCpAndCity(String cpAndCity) {
		this.cpAndCity = cpAndCity;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public void setInvoices(List<Invoice> invoices) {
		this.invoices = invoices;
	}

	public List<Purshase> getPurshases() {
		return purshases;
	}

	public void setPurshases(List<Purshase> purshases) {
		this.purshases = purshases;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	
}