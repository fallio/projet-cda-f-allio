package fr.eql.ai110.maisonviande.entity;

import java.util.ArrayList;
import java.util.List;

public class Cart {

	private List<Item> items = new ArrayList<Item>();
	
	private Integer totalAmount;
	
	public void addToItems(Item item) {
		this.items.add(item);
	}

	public Cart() {
		super();
	}

	public Cart(ArrayList<Item> items, Integer totalAmount) {
		super();
		this.items = items;
		this.totalAmount = totalAmount;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	
	
}


