package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="payment")
public class Payment implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="method")
	private String method;
	@Column(name="payment_date")
	private Date paymentDate;
	@Column(name="amount")
	private Float amount;
	
	@OneToOne
	@JoinColumn(referencedColumnName = "id")
	private Purshase purshase;

	public Payment() {
		super();
	}

	public Payment(Integer id, String method, Date paymentDate, Float amount, Purshase purshase) {
		super();
		this.id = id;
		this.method = method;
		this.paymentDate = paymentDate;
		this.amount = amount;
		this.purshase = purshase;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public Purshase getPurshase() {
		return purshase;
	}

	public void setPurshase(Purshase purshase) {
		this.purshase = purshase;
	}


}
