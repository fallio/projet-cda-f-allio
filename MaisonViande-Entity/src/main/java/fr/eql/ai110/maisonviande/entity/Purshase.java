package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "purshase")
public class Purshase implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="purchase_date")
	private LocalDate purchaseDate;
	@Column(name="total_amount")
	private Double totalAmount;
	@Column(name="total_amount_parsed")
	private String totalAmountParsed;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Statut statut;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Payment payment;
	@OneToOne
	@JoinColumn(referencedColumnName = "id")
	private Invoice invoice;
	
	@OneToMany(mappedBy = "purshase", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<CommandLign> commandLigns;
	

	public Purshase() {
		super();
	}


	public Purshase(Integer id, LocalDate purchaseDate, Statut statut, User user, Payment payment, Invoice invoice,
			List<CommandLign> commandLigns) {
		super();
		this.id = id;
		this.purchaseDate = purchaseDate;
		this.statut = statut;
		this.user = user;
		this.payment = payment;
		this.invoice = invoice;
		this.commandLigns = commandLigns;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}


	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}


	public Statut getStatut() {
		return statut;
	}


	public void setStatut(Statut statut) {
		this.statut = statut;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Payment getPayment() {
		return payment;
	}


	public void setPayment(Payment payment) {
		this.payment = payment;
	}


	public Invoice getInvoice() {
		return invoice;
	}


	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}


	public List<CommandLign> getCommandLigns() {
		return commandLigns;
	}


	public void setCommandLigns(List<CommandLign> commandLigns) {
		this.commandLigns = commandLigns;
	}


	public Double getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}


	public String getTotalAmountParsed() {
		return totalAmountParsed;
	}


	public void setTotalAmountParsed(String totalAmountParsed) {
		this.totalAmountParsed = totalAmountParsed;
	}


	
	
}
