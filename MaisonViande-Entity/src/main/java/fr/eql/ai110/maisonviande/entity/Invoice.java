package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoice")
public class Invoice implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="num_and_street_name")
	private String numAndStreetName;
	@Column(name="ht_amount")
	private Float hTAmount;
	@Column(name="ttc_amount")
	private Float tTCAmount;
	@Column(name="tva_Amount")
	private Float tVAAmount;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private TVARates tVaRates;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	
	@OneToOne
	@JoinColumn(referencedColumnName = "id")
	private Purshase purshase;

	public Invoice() {
		super();
	}

	public Invoice(Integer id, String numAndStreetName, Float hTAmount, Float tTCAmount, Float tVAAmount,
			TVARates tVaRates, User user, Purshase purshase) {
		super();
		this.id = id;
		this.numAndStreetName = numAndStreetName;
		this.hTAmount = hTAmount;
		this.tTCAmount = tTCAmount;
		this.tVAAmount = tVAAmount;
		this.tVaRates = tVaRates;
		this.user = user;
		this.purshase = purshase;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumAndStreetName() {
		return numAndStreetName;
	}

	public void setNumAndStreetName(String numAndStreetName) {
		this.numAndStreetName = numAndStreetName;
	}

	public Float gethTAmount() {
		return hTAmount;
	}

	public void sethTAmount(Float hTAmount) {
		this.hTAmount = hTAmount;
	}

	public Float gettTCAmount() {
		return tTCAmount;
	}

	public void settTCAmount(Float tTCAmount) {
		this.tTCAmount = tTCAmount;
	}

	public Float gettVAAmount() {
		return tVAAmount;
	}

	public void settVAAmount(Float tVAAmount) {
		this.tVAAmount = tVAAmount;
	}

	public TVARates gettVaRates() {
		return tVaRates;
	}

	public void settVaRates(TVARates tVaRates) {
		this.tVaRates = tVaRates;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Purshase getPurshase() {
		return purshase;
	}

	public void setPurshase(Purshase purshase) {
		this.purshase = purshase;
	}

	
	
}
