package fr.eql.ai110.maisonviande.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product implements Serializable{

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return Objects.equals(id, other.id);
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="weight")
	private String weight;
	@Column(name="description", columnDefinition = "TEXT")
	private String description;
	@Column(name="image")
	private String image;
	@Column(name="ht_amount")
	private Float hTAmount;
		
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Unit unit;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private SubCategory subCategory;
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<CommandLign> commandLigns;

	public Product() {
		super();
	}

	public Product(Integer id, String name, String description, String image, Float hTAmount, Unit unit,
			SubCategory subCategory, List<CommandLign> commandLigns) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.image = image;
		this.hTAmount = hTAmount;
		this.unit = unit;
		this.subCategory = subCategory;
		this.commandLigns = commandLigns;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Float gethTAmount() {
		return hTAmount;
	}

	public void sethTAmount(Float hTAmount) {
		this.hTAmount = hTAmount;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}

	public List<CommandLign> getCommandLigns() {
		return commandLigns;
	}

	public void setCommandLigns(List<CommandLign> commandLigns) {
		this.commandLigns = commandLigns;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	
}
