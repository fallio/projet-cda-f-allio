package fr.eql.ai110.maisonviande.ibusiness;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.CommandLign;
import fr.eql.ai110.maisonviande.entity.Purshase;

public interface PurshaseIBusiness {
	
	void addPurshase (Purshase purshase);
	
	void addCommandLign (CommandLign commandLign);

	Purshase getLastEntry();

	List<Purshase> getAll();

	String doubleTotalAmountParsed(Double arg);

}
