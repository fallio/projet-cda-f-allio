package fr.eql.ai110.maisonviande.ibusiness;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Category;

public interface CategoryIBusiness {

	List<Category> getCategories() ;

}
