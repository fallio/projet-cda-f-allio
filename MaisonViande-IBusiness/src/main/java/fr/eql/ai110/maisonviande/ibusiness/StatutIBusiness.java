package fr.eql.ai110.maisonviande.ibusiness;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Statut;

public interface StatutIBusiness {
	
	List<Statut> getStatuts() ;
}
