package fr.eql.ai110.maisonviande.ibusiness;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.entity.Product;

public interface ProductIBusiness {

	List<Product> getProductsFromCategory(Category category);
	
}
