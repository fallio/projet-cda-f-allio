package fr.eql.ai110.maisonviande.ibusiness;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Unit;

public interface UnitIBusiness {
	
	List<Unit> getUnits() ;
}
