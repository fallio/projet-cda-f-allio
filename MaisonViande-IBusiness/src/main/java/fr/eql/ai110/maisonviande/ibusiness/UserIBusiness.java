package fr.eql.ai110.maisonviande.ibusiness;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.User;

public interface UserIBusiness {

	List<User> getAll();
	
	void create(User user, String password);
	
	User connect(String login, String password);

	List<Purshase> getPurshasesByUser(User user);
}
