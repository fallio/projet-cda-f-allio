package fr.eql.ai110.maisonviande.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.entity.Product;
import fr.eql.ai110.maisonviande.entity.Unit;
import fr.eql.ai110.maisonviande.entity.User;
import fr.eql.ai110.maisonviande.ibusiness.CategoryIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.ProductIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.UnitIBusiness;

@ManagedBean(name="mbDisplay")
@SessionScoped
public class DisplayManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<Category> categories;
	private List<Product> products;
	private List<Unit> Units;
	private Product detailledProducts;
	private Category categorySelected;
	

	@EJB
	private CategoryIBusiness categoryBusiness;
	@EJB
	private ProductIBusiness productBusiness;
	@EJB
	private UnitIBusiness unitBusiness;

	@PostConstruct
	public void init() {
		User user = new User();
		categories = categoryBusiness.getCategories();
		Units = unitBusiness.getUnits();
		FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        user = (User) session.getAttribute("connectedClient");

        try {
        	System.out.println("-------------- User en session : "+ user.getLogin() + "-------------------------") ;
		} catch (Exception e) {
			System.out.println("-------------- Pas d'User en Session ------------------------>"+ user);
		}
		
	}

	public String detailCategory(Category category) {
		String forward = "/categorydetail.xhtml?faces-redirection=true";
		products = productBusiness.getProductsFromCategory(category);
		categorySelected = category;
		return forward;
	}
	
	public String detailProduct(Product products) {
		String forward = "/productdetail.xhtml?faces-redirection=true";
		detailledProducts = products;
		return forward;
	}
	
	
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Product getDetailledProducts() {
		return detailledProducts;
	}

	public void setDetailledProducts(Product detailledProducts) {
		this.detailledProducts = detailledProducts;
	}

	public Category getCategorySelected() {
		return categorySelected;
	}

	public void setCategorySelected(Category categorySelected) {
		this.categorySelected = categorySelected;
	}

	public List<Unit> getUnits() {
		return Units;
	}

	public void setUnits(List<Unit> units) {
		Units = units;
	}
	
	
}
