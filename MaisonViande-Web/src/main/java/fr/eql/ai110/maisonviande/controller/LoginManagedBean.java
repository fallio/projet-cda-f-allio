package fr.eql.ai110.maisonviande.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.User;
import fr.eql.ai110.maisonviande.ibusiness.UserIBusiness;

@ManagedBean(name="mbLogin")
@SessionScoped
public class LoginManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<User> users;
	
	private String login;
	private String password;
	private String name;
	private String surname;
	
	private User user;
	
	private Boolean isConnected = false;

	@EJB
	private UserIBusiness userBusiness;
	
	@PostConstruct
	public void init() {
		User user = new User();
		users = userBusiness.getAll();
		FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        user = (User) session.getAttribute("connectedClient");
        
        try {
        	System.out.println("-------------- User en session : "+ user.getLogin() + "-------------------------") ;
		} catch (Exception e) {
			System.out.println("-------------- Pas d'User en Session ------------------------>"+ user);
		}
	}

	public String create() {
		String forward = "/login.xhtml?faces-redirection=true";

		// instancier un Utilisateur :
		User user = new User();
		user.setLogin(login);
		user.setName(name);
		user.setSurname(surname);

		// enregistrer le nouvel utilisateur (business)
		userBusiness.create(user, password);
		System.out.println("ManagedBeanLevel :" + password);

		return forward;
	}


	public String connect() {
		String forward = null;
		System.out.println("*********************************Methode connect*******************************************");
		setUser(userBusiness.connect(login, password));
		if (user != null) {
			isConnected = true;
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
			session.setAttribute("connectedClient", user);
			forward = "/account.xhtml?faces-redirection=true";
		} else {
			isConnected = false;
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN, 
					"Identifiant et/ou mot de passe incorrect(s)",
					"Identifiant et/ou mot de passe incorrect(s)"
					);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
			forward = "/login.xhtml?faces-redirection=false";
		}
		try {
			System.out.println("------------------------- valeur user : "+ user.getLogin() + " ------------------------");
		} catch (Exception e) {
			System.out.println("------------------------- valeur user : "+ user + " ------------------------");
		}
		
		return forward;
	}

	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		setUser(null);
		return "/logout.xhtml?faces-redirection=true";
	}
	
	public List<Purshase> getPurshaseByUser() {
		
		return userBusiness.getPurshasesByUser(user);
	}
	
	//GETTERS & SETTERS

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserIBusiness getUserBusiness() {
		return userBusiness;
	}

	public void setUserBusiness(UserIBusiness userBusiness) {
		this.userBusiness = userBusiness;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Boolean getIsConnected() {
		return isConnected;
	}

	public void setIsConnected(Boolean isConnected) {
		this.isConnected = isConnected;
	}	
	
	

}
