package fr.eql.ai110.maisonviande.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.maisonviande.entity.Cart;
import fr.eql.ai110.maisonviande.entity.CommandLign;
import fr.eql.ai110.maisonviande.entity.Item;
import fr.eql.ai110.maisonviande.entity.Product;
import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.Statut;
import fr.eql.ai110.maisonviande.entity.User;
import fr.eql.ai110.maisonviande.ibusiness.CategoryIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.PurshaseIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.StatutIBusiness;
import fr.eql.ai110.maisonviande.ibusiness.UserIBusiness;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean(name="mbPurshase")
@SessionScoped
public class PurshaseManageBean implements Serializable {


	private static final long serialVersionUID = 1L;

	private User user;
	private List<Purshase> purshases;
	private Double totalAmount = 0.0;

	private String totalAmountParsed ="";

	private Integer quantity;
	private Cart cart = new Cart();
	private List<Statut> statuts;

	private List<Item> items = new ArrayList<Item>();

	@EJB
	private CategoryIBusiness categoryBusiness;
	@EJB
	private PurshaseIBusiness purshaseBusiness;
	@EJB
	private StatutIBusiness statutBusiness;
	@EJB
	private UserIBusiness userBusiness;

	@PostConstruct
	public void init() {
		totalAmountParsed = "0.00";
		statuts = statutBusiness.getStatuts();

		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		user = (User) session.getAttribute("connectedClient");
		purshases = purshaseBusiness.getAll();

		try {
			System.out.println("-------------- User en session : "+ user.getLogin() + "-------------------------") ;
		} catch (Exception e) {
			System.out.println("-------------- Pas d'User en Session ------------------------>"+ user);
		}
	}

	public String addToCart(Product product) {
		String forward = "/categorydetail.xhtml?faces-redirection=true";
		Item item = new Item();
		item.setProduct(product);
		item.setQuantity(quantity);


		items = cart.getItems();
		Boolean doesProductExist = false;

		for (Item itemTest : items) {
			if(itemTest.getProduct().equals(product)) {
				itemTest.setQuantity(itemTest.getQuantity()+quantity);
				doesProductExist = true;

				totalAmount = totalAmount + (item.getProduct().gethTAmount().floatValue()*item.getQuantity());
				System.out.println(totalAmount.toString());

				System.out.println("**************************************** MAJ Panier **************************************");
				
				FacesMessages.info("La quantité " , "à été mise à jours");
			}
		}
		if (doesProductExist == false) {

			totalAmount = totalAmount + (item.getProduct().gethTAmount().floatValue()*item.getQuantity());
			System.out.println(totalAmount.toString());

			cart.addToItems(item);
			System.out.println("**************************************** Nouveau(x) produit(s) ajouté(s) au panier **************************************");
			
			FacesMessages.info(item.getProduct().getName().toString(), " à été ajouté au panier");
		}

		try {
			System.out.println("-------------- User en session : "+ user.getLogin() + "-------------------------") ;
		} catch (Exception e) {
			System.out.println("-------------- Pas d'User en Session ------------------------>"+ user);
		}
		totalAmountParsed = purshaseBusiness.doubleTotalAmountParsed(totalAmount);
		
		return forward;		
	}

	public String validatePurshase() {
		String forward = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		user = (User) session.getAttribute("connectedClient");

		try {
			System.out.println("---------------- " + user.getLogin() + " is on session ----------------------");
		} catch (Exception e) {
			System.out.println("!!!!!!!!!----------------- is no USER on session ----------!!!!!!!!!!!!!!!!!!!!");
		}
		
		System.out.println("total dans le panier : " + totalAmount.toString());
		
		{if(totalAmount <= 0.0) {
			System.out.println("******************************Panier vide !************************");
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN, 
					"Votre panier est vide",
					"Votre panier est vide"
					);
			FacesContext.getCurrentInstance().addMessage("inpPanierLog", facesMessage);
			forward = "/panier.xhtml?faces-redirection=false";
		}else {		
			if (user != null) {
				Purshase purshase = new Purshase();
				purshase.setPurchaseDate(LocalDate.now());
				purshase.setStatut(statuts.get(0));
				purshase.setUser(user);
				purshase.setTotalAmount(totalAmount);
				purshase.setTotalAmountParsed(totalAmountParsed);
				purshaseBusiness.addPurshase(purshase);

				Purshase lastPurshaseEntry = purshaseBusiness.getLastEntry();
				System.out.println(lastPurshaseEntry.getId());


				for (Item item : items) {
					CommandLign commandLign = new CommandLign();
					commandLign.setQuantity(item.getQuantity());
					commandLign.setProduct(item.getProduct());
					commandLign.setPurshase(lastPurshaseEntry);
					purshaseBusiness.addCommandLign(commandLign);

				} 
				forward = "/purshaseRecap.xhtml?faces-redirection=true";
			}else {
			System.out.println("******************************Connection Requise************************");
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN, 
					"Connection requise",
					"Connection requise"
					);
			FacesContext.getCurrentInstance().addMessage("inpPanierEmpty", facesMessage);
			forward = "/panier.xhtml?faces-redirection=false"; 
		}
		}
		}
		return forward;
	}

	public String ShoppingCartEmpty() {
		setCart(null);
		totalAmount = 0.0;
		totalAmountParsed = purshaseBusiness.doubleTotalAmountParsed(totalAmount);
		setCart(cart=new Cart());
		FacesMessages.info("Votre panier ", "est vide.");
		return "/panier.xhtml?faces-redirection=true";
	}

	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<Statut> getStatuts() {
		return statuts;
	}
	public void setStatuts(List<Statut> statuts) {
		this.statuts = statuts;
	}
	public List<Purshase> getPurshase() {
		return purshases;
	}
	public void setPurshase(List<Purshase> purshase) {
		this.purshases = purshase;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalAmountParsed() {
		return totalAmountParsed;
	}

	public void setTotalAmountParsed(String totalAmountParsed) {
		this.totalAmountParsed = totalAmountParsed;
	}




}
