package fr.eql.ai110.maisonviande.idao;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Category;
import fr.eql.ai110.maisonviande.entity.Product;

public interface ProductIDao extends GenericIDao<Product> {
	
	List<Product> productsFromCategory(Category category);

	
}
