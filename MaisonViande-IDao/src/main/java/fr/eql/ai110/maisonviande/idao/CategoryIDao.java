package fr.eql.ai110.maisonviande.idao;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Category;

public interface CategoryIDao extends GenericIDao<Category> {

	List<Category> getCategories();

}
