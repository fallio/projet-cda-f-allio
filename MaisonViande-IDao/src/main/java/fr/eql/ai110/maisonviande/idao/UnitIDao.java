package fr.eql.ai110.maisonviande.idao;

import fr.eql.ai110.maisonviande.entity.Unit;

public interface UnitIDao extends GenericIDao<Unit> {

}
