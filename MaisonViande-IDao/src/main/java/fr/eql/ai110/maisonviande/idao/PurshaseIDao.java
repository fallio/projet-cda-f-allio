package fr.eql.ai110.maisonviande.idao;

import fr.eql.ai110.maisonviande.entity.Purshase;

public interface PurshaseIDao extends GenericIDao<Purshase> {

	Purshase getLastEntry();
	
}
