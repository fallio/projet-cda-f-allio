package fr.eql.ai110.maisonviande.idao;

import java.util.List;

import fr.eql.ai110.maisonviande.entity.Purshase;
import fr.eql.ai110.maisonviande.entity.User;

public interface UserIDao extends GenericIDao<User> {

	User authenticate(String login,String password);
	
	String getSalt(String login);

	List<Purshase> getPurshasesByUserId(User user);

}
