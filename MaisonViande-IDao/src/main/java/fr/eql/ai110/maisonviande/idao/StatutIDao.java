package fr.eql.ai110.maisonviande.idao;

import fr.eql.ai110.maisonviande.entity.Statut;

public interface StatutIDao extends GenericIDao<Statut> {

}
